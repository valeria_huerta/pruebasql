package pruebasql;

import java.sql.*;

/**
 *
 * @author diego
 */
public class PruebaSQL {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // El usuario con el que nos conectaremos.
        String USER = "root";
        // La contrasenia asociada con el usuario.
        String PASSWORD = "root";
        // Servidor con el que nos conectaremos.
        String SERVER = "localhost";
        // Puerto que utiliza mysql
        String PORT = "3306";
        // Base de datos con la que nos vamos a conectar.
        String DATABASE = "PruebaSQL";
        // URL con la que vamos a establecer la conexion.
        String URL = "jdbc:mysql://" + SERVER + ":" + PORT + "/" + DATABASE;
        // Objeto con el que estableceremos la conexion.
        Connection conn = null;
        
        try{
            // Tratamos deestablecer la conexion con los datos dados.
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Conexion exitosa.");
            // Objeto que utilizaremos para enviar la consulta a la base de datos.
            Statement stmt = (Statement)conn.createStatement();
            // Valor para la columna nombre.
            String nombre = "Computadora";
            // Valor para la columna ciudad.
            String ciudad = "Ciudad de Mexico";
            // Creamos la consulta, en este caso agregar un registro.
            String insert = "INSERT INTO Articulos (nombre, ciudad) VALUES ('" + nombre + "', '" + ciudad + "');";
            // Mandamos a ejecutar la consulta.
            stmt.executeUpdate(insert);
        } catch(SQLException e){
            System.err.println(e);
        }
    }
    
}
